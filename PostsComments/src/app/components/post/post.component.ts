import { PostsService } from "./../../services/posts.service";
import { Post } from "./../../models/post.model";
import { Component, OnInit, Input } from "@angular/core";

@Component({
  selector: "app-post",
  templateUrl: "./post.component.html",
  styleUrls: ["./post.component.css"]
})
export class PostComponent implements OnInit {
  @Input() idUser: number;
  formObject: Post;
  postList: Post[];
  constructor(private postsService: PostsService) {}

  ngOnInit() {
    this.postsService.getPostsByUserId(this.idUser).subscribe(result => {
      this.postList = result;
    });
  }
}
