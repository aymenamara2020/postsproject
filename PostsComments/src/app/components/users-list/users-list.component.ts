import { Component, OnInit } from "@angular/core";
import { UsersListService } from "../../services/users-list.service";
import { Router } from "@angular/router";
import { User } from "../../models/user.model";
import {
  NgbModal,
  NgbActiveModal,
  ModalDismissReasons,
  NgbModalOptions
} from "@ng-bootstrap/ng-bootstrap";
import { analyzeFileForInjectables } from "@angular/compiler";

@Component({
  selector: "app-users-list",
  templateUrl: "./users-list.component.html",
  styleUrls: ["./users-list.component.css"]
})
export class UsersListComponent implements OnInit {
  users: User[];
  message: string;
  closeResult: string;
  modalOptions: NgbModalOptions;
  showUserPost: number[];

  constructor(
    private usersListService: UsersListService,
    private router: Router,
    private modalService: NgbModal
  ) {}

  getAllUsers() {
    this.usersListService.retriveAllUsers().subscribe(response => {
      this.users = response;
      console.log(response);
    });
  }

  ngOnInit() {
    this.showUserPost = [];
    this.getAllUsers();
  }

  deleteUser(id) {
    console.log(`delete ${id}`);
    this.usersListService.deleteUser(id).subscribe(response => {
      /*  this.users = response;
      this.message = `Success! vous avez Supprimer L'Utilisateur' ${id}`;
      */
      console.log(response);
      this.RefrechUser();
    });
  }

  RefrechUser() {
    this.usersListService.retriveAllUsers().subscribe(response => {
      this.users = response;
    });
  }

  updateUser(id) {
    this.router.navigate(["users", id]);
  }
  AddUser() {
    this.router.navigate(["users", -1]);
  }

  showVar: boolean = true;

  show(id: number) {
    if (id) {
      console.log("executed");
      if (this.showUserPost.includes(id)) {
        this.showUserPost.splice(this.showUserPost.indexOf(id), 1);
        return false;
      } else {
        this.showUserPost.push(id);
        return true;
      }
    }
  }

  toggleChild(user: User) {
    console.log(user);
    user.showPost = !user.showPost;
  }
  edit(user) {
    user.showEdit = !user.showEdit;
  }
  open() {}
}
