import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";

import { Post } from "../models/post.model";

const httpOptions = {
  headers: new HttpHeaders({
    "Content-Type": "application/json; charset=UTF-8"
  })
};

@Injectable()
export class PostsService {
  private postsUrl: string = "https:/localhost:8080/posts";

  constructor(private http: HttpClient) {}

  getPostsByUser(id: number): Observable<Post> {
    return this.http.get<Post>(`${this.postsUrl}/${id}`);
  }

  getPosts(): Observable<Post[]> {
    return this.http.get<Post[]>(`${this.postsUrl}?_sort=views&_order=desc`);
  }

  getPostsByUserId(id: number): Observable<Post[]> {
    return this.http.get<Post[]>(`${this.postsUrl}?userId=${id}`);
  }

  addPost(post: Post): Observable<Post> {
    return this.http.post<Post>(this.postsUrl, post, httpOptions);
  }
}
