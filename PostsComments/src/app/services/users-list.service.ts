import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { User } from "../models/user.model";

@Injectable({
  providedIn: "root"
})
export class UsersListService {
  constructor(private http: HttpClient) {}

  retriveAllUsers() {
    return this.http.get<User[]>(`http://localhost:8080/users`);
  }

  deleteUser(id) {
    return this.http.delete<User[]>(`https://localhost:8080/users/${id}`);
  }

  updateUser(user) {
    return this.http.put(`https://localhost:8080/users/update`, user);
  }

  retriveUser(id) {
    return this.http.get<User>(`http://localhost:8080/users/${id}`);
  }

  createUser(users) {
    return this.http.post(`http://localhost:8080/users`, users);
  }
  deletSessionUser(idu, ids) {
    return this.http.delete<User[]>(
      `http://localhost:8202/users/${idu}/Sessions/${ids}`
    );
  }
}
