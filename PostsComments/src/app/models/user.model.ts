export class User {
  id: number;
  name: string;
  email: string;
  phone: number;
  showPost: boolean;
  showEdit: boolean;

  /*
  private String id;
private String name;
private String email;
private String phone;
  */
}
