import { Component, OnInit, Input } from "@angular/core";
import { User } from "../models/user.model";
import { UsersListService } from "../services/users-list.service";

@Component({
  selector: "app-edit-component",
  templateUrl: "./edit-component.component.html",
  styleUrls: ["./edit-component.component.css"]
})
export class EditComponentComponent implements OnInit {
  @Input() user: User;
  formObject: User;
  constructor(private usersListService: UsersListService) {}

  ngOnInit() {
    this.formObject = Object.assign({}, this.user);
  }

  update() {
    console.log(this.formObject.name);
    this.usersListService.updateUser(this.formObject);
  }
}
