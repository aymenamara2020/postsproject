package com.example.demo.repositories;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

import com.example.demo.models.User;
@Configuration
@RepositoryRestResource
public interface UserRepository extends MongoRepository<User, String> {
}
