package com.example.demo.repositories;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.example.demo.models.Post;
@Configuration
@RepositoryRestResource
public interface PostRepository extends MongoRepository<Post, String>{
}
