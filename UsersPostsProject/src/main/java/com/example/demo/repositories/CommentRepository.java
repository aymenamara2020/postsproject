package com.example.demo.repositories;


import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.example.demo.models.Comment;
@Configuration
@RepositoryRestResource
public interface CommentRepository extends MongoRepository<Comment, String>{	

}
