package com.example.demo.models;



import java.util.ArrayList;
import java.util.Collection;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;



@Document("user")
public class User{
private String id;
private String name;
private String email;
private String phone;
@DBRef(lazy = true) 
private Collection<Post> Posts = new ArrayList<>();
public User() {}

public User(String id, String name, String email, String phone, Collection<Post> posts) {
	super();
	this.id = id;
	this.name = name;
	this.email = email;
	this.phone = phone;
	Posts = posts;
}

public String getId() {
	return id;
}
public void setId(String id) {
	this.id = id;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}

public String getEmail() {
	return email;
}

public void setEmail(String email) {
	this.email = email;
}

public String getPhone() {
	return phone;
}

public void setPhone(String phone) {
	this.phone = phone;
}
public Collection<Post> getPosts() { return this.Posts; } 
public void setPosts(Collection<Post> Posts) { this.Posts = Posts; }

}
