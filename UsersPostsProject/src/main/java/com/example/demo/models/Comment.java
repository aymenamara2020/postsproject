package com.example.demo.models;

import org.springframework.data.mongodb.core.mapping.Document;

@Document("comment")
public class Comment {	
	private String id;
	private String body;
	
	public Comment(String id, String body) {
		super();
		this.id = id;
		this.body = body;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
}
