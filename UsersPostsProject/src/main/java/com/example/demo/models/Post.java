package com.example.demo.models;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Document("post")
public class Post {
private String id;
private String title;
private String body;
@DBRef(lazy = true) private Collection<Comment> Comments = new ArrayList<>();
public Post() {	
}

public Post(String id, String title, String body, Collection<Comment> comments) {
	super();
	this.id = id;
	this.title = title;
	this.body = body;
	Comments = comments;
}

public String getId() {
	return id;
}
public void setId(String id) {
	this.id = id;
}
public String getBody() {
	return this.body;
}
public void setBody(String body) {
	this.body = body;
}

public String getTitle() {
	return title;
}
public void setTitle(String title) {
	this.title = title;
}

public Collection<Comment> getComments() { return this.Comments; } 
public void setPosts(Collection<Comment> Comments) { this.Comments = Comments; }


}
