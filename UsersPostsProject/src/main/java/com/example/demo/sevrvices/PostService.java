package com.example.demo.sevrvices;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.models.Post;
import com.example.demo.repositories.PostRepository;

@Service
public class PostService {
	@Autowired

	private final PostRepository postRepository;
public PostService(PostRepository postRepository) {
this.postRepository = postRepository;
}

public Post savePost(Post post) {
	return postRepository.save(post);	
}

public List<Post>  getPosts() {
	return postRepository.findAll();
	
}

}
