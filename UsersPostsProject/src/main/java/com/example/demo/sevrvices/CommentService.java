package com.example.demo.sevrvices;

import java.util.List;

import org.springframework.stereotype.Service;

import com.example.demo.models.Comment;
import com.example.demo.repositories.CommentRepository;


@Service
public class CommentService {
	
	private final CommentRepository commentRepository;
public CommentService(CommentRepository commentRepository) {
this.commentRepository = commentRepository;
}

public Comment saveComment(Comment comment) {
	return commentRepository.save(comment);	
}

public List<Comment> getAll(){
return commentRepository.findAll();
}



}
