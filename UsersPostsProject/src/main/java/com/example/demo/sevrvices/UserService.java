package com.example.demo.sevrvices;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.data.mongodb.core.MongoTemplate;
import com.example.demo.models.User;
import com.example.demo.repositories.UserRepository;

@Service
public class UserService {
	@Autowired
private final UserRepository userRepository;
	@Autowired
	private MongoTemplate mongoTemplate;
public UserService(UserRepository userRepository) {
	this.userRepository = userRepository;
}


public List<User> getUsers() {
	//Optional in java 8
	return userRepository.findAll();
}

public User saveUser(User user) {
	return userRepository.save(user);
}

public User deleteById(long id) {
	User user =getUserById(id);
	if(user==null) {
		return null;
		}else {
			this.mongoTemplate.remove(user);
			
			return user;
		}
}

public User getUserById(long id) {
	Query query = new Query();
	query.addCriteria(Criteria.where("_id").is(id));
	return mongoTemplate.findOne(query, User.class);
}

public User update(User user){
    Query query = new Query();
    query.addCriteria(Criteria.where("id").is(user.getId()));
    Update update = new Update();
    update.set("name", user.getName());
    
    return mongoTemplate.findAndModify(query, update, User.class);
}


public User updateOneUser(User user) {
	mongoTemplate.save(user);
	   return user;
}







}
