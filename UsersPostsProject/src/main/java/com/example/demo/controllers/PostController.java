package com.example.demo.controllers;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.models.Post;
import com.example.demo.sevrvices.PostService;
@RestController
public class PostController {
	private final PostService postService;
	public PostController(PostService postService) {		
		this.postService = postService;
	}
	@GetMapping("/posts")
	public List<Post> getPosts(){
		return postService.getPosts();
	}
	@PostMapping("/addPost")
	public Post addPost(@RequestBody Post post) {
			return postService.savePost(post);
	}

}
