package com.example.demo.controllers;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.example.demo.models.Post;
import com.example.demo.models.User;
import com.example.demo.sevrvices.PostService;
import com.example.demo.sevrvices.UserService;

@CrossOrigin(origins="http://localhost:4200")
@RestController
@RequestMapping(value = "/users")
public class UserController {
@Autowired
private final UserService userService;
private final PostService postService;
public UserController(UserService userService, PostService postService) {
	this.userService = userService;
	this.postService = postService;
}
@GetMapping("")
public List<User> getuser(){	
	return userService.getUsers();
}
//supprimer user
@DeleteMapping("/users/{id}")
public ResponseEntity<Void> deleteUser(@PathVariable long id){
	User user = userService.deleteById(id);
	if(user!=null) {
		return ResponseEntity.noContent().build();
	}
	return ResponseEntity.notFound().build();
}
//Modifier utlisateur
@PutMapping(value="/users/{id}" )
	public ResponseEntity<User> updateUser(@RequestBody User user, @PathVariable long id ){
	 		User userUpdated =  userService.saveUser(user);
			return  new ResponseEntity<User> (user,HttpStatus.OK);
	}	
// ajouter utilisteur
	@RequestMapping(value = "/users", method = RequestMethod.POST)
	public User addNewUsers(@RequestBody User user) {
		/*LOG.info("Saving user.");*/
return userService.saveUser(user);
	}
/*
@GetMapping("/{userId}/posts")
public List<Post> getPosts(@PathVariable String userId){
	
	return postService.getUserPosts(userId);
}
@PostMapping("/user")
public User addUser(@RequestBody User user) {
	return userService.saveUser(user);
}
*/
}
