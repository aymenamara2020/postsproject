package com.example.demo.controllers;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import com.example.demo.models.Comment;
import com.example.demo.sevrvices.CommentService;
@RestController
public class CommentController {
	private final CommentService commentService;
	public CommentController(CommentService commentService) {
		this.commentService = commentService;
	}
	@GetMapping("{postId/comments}")
	public List<Comment> getCommentsByPostId(@PathVariable String postId) {
		return null;
	}
	@GetMapping("/comments")
	public List<Comment> getComments() {
		return this.commentService.getAll();
	}

}
