package com.example.demo;



import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;

import com.example.demo.models.User;
import com.example.demo.sevrvices.UserService;


@Configuration
@SpringBootApplication
public class UsersPostsProjectApplication implements CommandLineRunner {


	
	public static void main(String[] args) {
		SpringApplication.run(UsersPostsProjectApplication.class, args);	
	}
	@Autowired
	private  UserService us;
	
@Override
	public void run(String... args) throws Exception {
		User nermine = new User("test", "test",  "test",  "test", new ArrayList<>());
	System.out.println(nermine);
	us.saveUser(nermine);	
	}

}
